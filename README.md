# The MuttWorld Guide to being an Ambassador #
## For Survival ##
Edited Jan. 2016
### By XXLordSXX ###

Ambassadors have a very important role as they represent the server to the players, and are often the first contact a new player has with the server. So you need to be kind, helpful, and friendly! As an Ambassador you will have to deal with all kinds of people, and a lot of difficult situations; however most of the time you can just play the game as you always have been while keeping an eye on chat. Of course, you should also keep an eye out for glitches/cheats and market problems, and report them to a mod/admin. Your job is to help new players with basic problems, like learning the commands on a server. Your job is NOT, to correct spelling, argue, fight with players, or make fun of players. Players should think of you as a helping friend, NOT a robotic chat Nazi. We want to stop bullies not be bullies.

Let us begin with going over some of the basic tenets of staffing here: 

We expect you to:

1)	Be friendly

2)	Be involved

3)	Be professional

4)	Be. Professional. – Yes, this is listed twice.

*Be friendly*: This above all. Say hey to new players and ask if they have any questions. Say hey to returning players when they sign back in. Join in the conversation in public chats and answer questions freely. When you are friendly with the players, they want to stick around and feel like they belong. The longer they stick around and the more they feel that this is “their” server, the more likely they are to donate. The more likely they are to donate, the longer we can keep being an awesome server (servers cost money).

*Being involved*: If a player is proud of something they’ve built or done, ask to see it. If a player would like an event, host whatever is in your power to do, or suggest to a mod that players would like an event hosted. Players will often get bored and feel as though they’ve done all that there is to do. An active and engaging community is key for these players particularly. It’s important to give players a reason to come back!

*Being professional* is the hardest part of being staff. Many of our players are young, and the ones who aren’t young, some of them are trolls. One trait that young players and trolls have in common? They are absolutely certain that they shouldn’t be in trouble, and when someone says they are, they either get upset or mad. If they decide they don’t like you, they’ll find reasons to make that known in as public a way as they can. All while they continue to expect you to save them from rule-breakers. It can be a challenge to stay nice and professional with these ne’er-do-wells, but that is a challenge you must meet head-on. If you’re having trouble dealing with someone, ask another staff member to sub in for you in staff chat.

Being professional isn’t just about how you deal with problem players. It’s also in how you interact with your fellow staff. We are a diverse group of people, with different personalities and interests. You’re bound to butt heads with someone eventually. DO NOT EVER do so on the server in any public chat. You are to try to work out the problem with the person directly first, in staff chat if you need a second opinion or using a senior staff member as a mediator if neither of those two options work. 

If you are incapable or unwilling to follow these simple guidelines, you may wish to ask an admin to demote you. We don’t mind having you play on our servers, but we have to make sure we have only the best people on staff.

**---Muting---**

You NEED to be the calming force on the server, so don't let name calling or rudeness get to you. If things get crazy, ask a mod for help. Mutes, or the threat of mute, is one of the ways an Ambassador controls the chat. Many times the threat of a mute will stop the bad behavior. Remember we don't WANT to mute our players, so use this power with discretion. But sometimes you may need to mute right away, other times you may warn a player several times. As an example: if a player is spamming an IP address just mute and let a mod know, or if a player bypasses the chat filters with swearing warn a few times then mute, then explain the rules. HOWEVER, if a player uses a 'slur' as an adjective, or spams a question over and over, warnings should be used before a mute. Never unmute because a player begs you to. If you show them that they can get their way by whining and crying and making excuses, you’ve lost your authority in their eyes and they will continue to try to see how much more they can get away with. Only unmute if you accidentally did not set a time limit, muted the wrong person with a Tab complete (PLEASE check your commands before you hit enter so this doesn’t happen.), or you had a calm and mature discussion with the player and you unmute on YOUR terms, not theirs. 

Often times, a mute can solve an argument that that has gotten out of hand. By careful judgement of the situation and muting one or both of the players who are the biggest problem, the argument can stop within seconds. In this case it is a good idea that you have a private chat with the player to explain why you muted them and the actions that can be taken by them to ensure it does not happen again.

So at this point you may be asking yourself…

**---  Spam and how much should I allow?  ---**

A death cry like, "ARRRRRRRRGGGGGGG!" posted ONE time in chat is not spam worth your time, and will start to make players dislike you if you constantly warn people for this, so don't. But posted multiple times like: "ARRRRRRRRGGGGGGG!" "ARRRRRRRRGGGGGGG!" "ARRRRRRRRGGGGGGG!" It becomes spam and needs to be dealt with. The same goes for caps. If it’s like this: "SAMMY!", let it go. BUT if it's like this, "SAMMY STOP KILLING ALL MY PIGS YOU ARE A BASTARD AND I HOPE YOU DIE!" It needs to be dealt with.

Bypassing the word filters is another thing that you need to keep a look out for. People find all kinds of creative ways to spell curse words, using * like sh*t or any number of other tricks. A warning or two followed by a mute is an easy way to get the point across that this is not tolerated. However, if role playing or some other game is going on, and there are a few caps or a little spam, don't stop everything to correct a small infraction.

**--- Professional interaction with players and staff ---**

A personal pet peeve of mine is when players AND ambassadors try to correct repeated letter spam such as “noooooooooooooooooooo “by requesting “less o’s plz”. And let me explain some of the problems with this response:

*Firstly* it accomplishes nothing. If they were to spam another letter instead, would it be okay? Address the root of the problem, not a symptom.

*Secondly* this is a very impulsive response, you’re seeing something in chat are responding without thinking it through. Ambassadors are required to be thoughtful and helpful, not sound like robots.

*Lastly*, using incorrect spelling and grammar such as “plz” is unprofessional. Remember the staff tenets. Prepare professional responses when addressing player problems with correct spelling and grammar. If you were working as a police officer and trying to work with a civilian during chaos, you would not say something like “plz dunt do that” you would say something more along the lines of “Sir, please remain calm while we address the problem. We will get back to you as soon as possible.” Grammar and spelling matters, and it WILL determine whether a player respects your authority. Ambassadors often find that their authority is undermined because they are not professional. You conduct is being noticed by both players and senior staff.

But, while you must always be on your best behavior, you can be much more relaxed when interacting with players for fun.

**--- Ambassadors and their role with players ---**

An important part of the Ambassador job is getting to know people. You will make many friends in your time as staff, and this friendship is important. Getting to meet a wide variety of people is the most fun part of being a staff member, and is the reason we play on Muttsworld, to have fun playing a game with a bunch of friends. Something as simple as telling a player that their building looks amazing can really make their day. Remember that behind every player is a person who may be young, old, happy, or going through a rough time. Take the time to talk to people and be a good listener. Ambassadors are the gateway between staff and the community, and remember to treat players with respect and kindness.

**---EXTRAS---**

So you log on to your favorite server and there are 2-3 other Ambassadors on and not much to do... If you have 'Ambassador' on one of our other servers this would be the perfect time to go there and see if you can help. If you log on to a server and it is full of players and the Ambassadors are overwhelmed, most of the time you can help even if you are not an Ambassador on that server.

Some tips to live by: 

- Always use "please" and "thank you" when moderating chat. This makes the players feel respected.

- If a player is disobeying the chat rules, don't just say "caps," or "spam." Tell the player what they are doing wrong with manners (this goes along with one of my previous points.)

- A player typically gets 1 warning, then it’s a short mute. Make it 2-4 minutes depending on the severity. It's to show that we aren't going to sit around and give warnings all day. 

- More severe infractions may be met with more severe mutes, up to 30 minutes or more for persistent rule breakers. 

- A player will sometimes ask to buy or sell items in global chat. Kindly direct them to the trade chat for this purpose. Likewise, if a player is holding a non-trade related conversation in the trade chat, request that they move to global or local chat to carry on their conversation.

- Memorize all of the basic server commands and help players out when they ask about them.

- It is your job to answer all player questions, not wait for a mod to do it. If an admin has to answer questions because you’re not paying attention, you’re failing your job. Granted, if you don’t know the answer, don’t make something up just to give them an answer. The best course of action is to talk to other staff privately in staff chat to see if someone does know the answer. If nobody is available to give you an answer, write it down and tell the player you’ll get back to them after finding out. And remember to get back to them with it as soon as possible. This will show the players that staff cares about their thoughts and questions.

**==Commands==**

- /mute <player> <time in minutes> EX: /mute cooleepicminer 5 - This will mute cooleepicminer for 5 Minutes /unmute <Player> EX: /unmute Michaelgamer350 - This will unmute Michaelgamer350 (Note: You need to put a number after the player name or they will be muted for longer than intended)
See who is in a channel: 
- /who <channelname> (If a player is complaining about an invisible player harassing them, you can inform them of this command.)

Doing Modreqs: Ambassadors are encouraged to handle any modreqs that can be performed without the need of a mod or admin (Questions, whenever a player modreqs the number "2", etc.). This will help the mods by clearing out unimportant tickets.

- Check for open modreqs by typing /check.
- To claim a ticket: /tk claim <#> EX: /tk claim 252
- When you are done with a ticket: /Done <#> <Comment> EX: /Done 252 "Zmoney will be banned for his griefing"
- If you accidentally claimed a ticket that requires a mod's assistance, simply unclaim the ticket: /tk unclaim <#> EX: /tk unclaim 252
- If you accidentally closed the wrong ticket (you should be careful when typing ticket numbers to avoid a mistake like this) you can reopen the ticket: /tk reopen <#> EX: /tk reopen 252
- There is a command which will claim the ticket AND teleport you to the spot that it was filed: /tick <#> EX: /tick 252


- /newestregion - An important role as an Ambassador is to help and welcome new players to the server. This may require you to visit the newest region every now and then and ask "Hello, everyone ok?" Remember, players will want to stay if we can give them a reason to. 

Along the same lines of welcoming new players is your responsibility to make them stay. Make the players feel at home and that this is a friendly server in which the staff cares for their players. For this reason we have two commands which ambassadors are encouraged to use liberally:

- /tpnext 
     
    and

- /tpprev

These two commands allow you to teleport to the players of the server in a cycle, with /tpnext teleporting you to the next person on the cycle, and /tpprev teleporting you to the previous person on the cycle. Use these commands to visit players. Show up, ask them how they're doing, comment on their build, stay and talk awhile. This will help players to get to know you. IMPORTANT- Take social cues when visiting players. If a player is obviously busy building and does not want to talk, wish them nice day and move along. There is nothing worse than feeling annoyed because of over persistent staff. Give players space when they want it.

Every now and then, you will get a player who is unknowingly talking in Global Chat, and will not listen to you when you ask them to switch to local chat. There’s a command for this! Remember to ask the player to move into local chat before forcing them to do so, as this will teach them how to switch channels if they don't already know. /fl <Playername> EX: /fl QuietQuilla

Lastly, and this goes with doing modreqs, ambassadors have the TEP command, allowing for you to be able to teleport to another player without them having to accept the request. This is useful when dealing with problems between 2 players, or, like previously stated, when dealing with modreqs. /tep <Player name> EX: /tep Liebelegof (WARNING: If you /tep to a mod, you run the risk of dying, as we can either be over lava or in the air)

The last thing to remember is that Ambassadors are responsible for trying to sell diplomat and other store items, as well as getting players to vote. Ambassadors have all of the commands that permanent diplomats have. This includes /pwarps, the teleport arrow, disguises, and pets. Remember to use these things often when interacting with players, and when they ask how to get it, inform them it's one of the many perks of being diplomat and direct them to the store (/buydiplo) In addition, remind players that they make all grief stop by purchasing land protection. This is how mumble makes money to eat and pay his bills, so it's a very important task!

We thank you for your help and don't forget to have fun! :-)